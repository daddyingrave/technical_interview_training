package homework1.task0_linked_list;

import java.util.Objects;

public class LinkedList<T> {

    private Node<T> first;
    private Node<T> last;
    private int size;

    public boolean insert(T element) {
        Objects.requireNonNull(element);

        if (size == 0) {
            first = new Node<>(element);
            first.setPrevious(null).setNext(null);
            size++;
            return true;
        }
        if (size == 1) {
            last = new Node<>(element);
            last.setPrevious(first).setNext(null);
            first.setNext(last);
            size++;
            return true;
        }
        Node<T> newNode = new Node<>(element);
        Node<T> lastPrevious = last;

        last = newNode;
        last.setPrevious(lastPrevious).setNext(null);
        lastPrevious.setNext(last);
        size++;

        return true;
    }

    public int size() {
        return size;
    }

    private static class Node<R> {

        private R element;
        private Node<R> previous;
        private Node<R> next;

        private Node(R element) {
            this.element = element;
        }

        public Node<R> setPrevious(Node<R> previous) {
            this.previous = previous;
            return this;

        }

        public Node setNext(Node<R> next) {
            this.next = next;
            return this;
        }

    }

}
