package homework1.task2_braces;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.Stack;

public class BracesChecker {

    private static final Stack<Character> tokens = new Stack<>();

    private static final Set<Character> openBraces;
    private static final Set<Character> closeBraces;

    static {
        openBraces = new HashSet<>();
        openBraces.add('[');
        openBraces.add('{');
        openBraces.add('(');

        closeBraces = new HashSet<>();
        closeBraces.add(']');
        closeBraces.add('}');
        closeBraces.add(')');
    }

    public static boolean isBalanced(String string) {
        Objects.requireNonNull(string);

        for (int i = 0; i < string.length(); i++) {
            Character character = string.charAt(i);
            if (openBraces.contains(character)) {
                tokens.push(character);
            } else if (closeBraces.contains(character)) {
                if (getBrace(tokens.peek()) == character)
                    tokens.pop();
                else
                    return false;
            }
        }
        return tokens.size() == 0;
    }

    private static char getBrace(char brace) {
        switch (brace) {
            case '[':
                return ']';
            case '{':
                return '}';
            case '(':
                return ')';
                default: return '0';
        }
    }

}
