package homework1.task3_array_queue;

import java.util.Objects;

public class ArrayQueue<T> {

    private Object[] elements;
    private int first;
    private int size;

    private static final double FREE_SPACE_THRESHOLD = 0.5;

    public ArrayQueue() {
        this(10);
    }

    public ArrayQueue(int capacity) {
        elements = new Object[capacity];
    }

    public boolean add(T o) {
        Objects.requireNonNull(o);

        if (isEnoughSpace()) {
            elements[size++] = o;
        } else {
            Object[] newArray = new Object[size * 3 / 2];
            System.arraycopy(elements, 0, newArray, 0, size);
            elements = newArray;
            newArray = null;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    public T remove() {
        if (size == 0) throw new IndexOutOfBoundsException();

        T elem = (T) elements[first];
        elements[first] = null;
        if (size > 1) first++;
        size--;

        if (isSparse() || isToSmall()) {
            Object[] newArray = new Object[elements.length * 2 / 3];
            System.arraycopy(elements, first, newArray, 0, first);
            elements = newArray;
            newArray = null;

            first = 0;
        }
        return elem;
    }

    @SuppressWarnings("unchecked")
    public T peek() {
        return (T) elements[first];
    }

    public int size() {
        return size;
    }

    private boolean isSparse() {
        return size != 0 && first / size > FREE_SPACE_THRESHOLD;
    }

    private boolean isEnoughSpace() {
        return size < elements.length;
    }

    private boolean isToSmall() {
        return (elements.length - first) < elements.length * 2 / 3;
    }

}
