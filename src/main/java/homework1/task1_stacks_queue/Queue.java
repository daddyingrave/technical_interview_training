package homework1.task1_stacks_queue;

import java.util.Objects;
import java.util.Stack;

public class Queue<T> {

    private Stack<T> firstStack = new Stack<>();
    private Stack<T> secondStack = new Stack<>();

    private int size;
    private boolean isReverse;

    public boolean insert(T elem) {
        Objects.requireNonNull(elem);

        if (!isReverse && firstStack.size() == 0) {
            isReverse = true;
            size++;
            return firstStack.push(elem) != null;
        } else {
            size++;
            return secondStack.push(elem) != null;
        }
    }

    public T remove() {
        if (isReverse) {
            isReverse = false;
            T elem = firstStack.pop();

            while (!secondStack.empty()) {
                firstStack.push(secondStack.pop());
            }
            size--;
            return elem;
        } else {
            size--;
            return firstStack.pop();
        }
    }

    public int size() {
        return size;
    }

}
