package homework1.task1_stacks_queue;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class QueueTest {

    private static Queue<String> preparedQueue() {
        Queue<String> queue = new Queue<>();
        queue.insert("Panda");
        queue.insert("told");
        queue.insert("us");
        queue.insert("story");
        return queue;
    }

    @Test
    public void insertMethodTest() {
        Queue<String> queue = new Queue<>();
        assertThat(queue.insert("Panda"), is(true));
    }

    @Test
    public void removeMethodTest() {
        Queue<String> queue = preparedQueue();

        assertThat(queue.remove(), is("Panda"));
        assertThat(queue.remove(), is("told"));
        assertThat(queue.remove(), is("us"));
        assertThat(queue.remove(), is("story"));
    }



    @Test
    public void sizeMethodTest() {
        Queue<String> queue = preparedQueue();
        assertThat(queue.size(), is(4));

        queue.remove();
        assertThat(queue.size(), is(3));
    }
}