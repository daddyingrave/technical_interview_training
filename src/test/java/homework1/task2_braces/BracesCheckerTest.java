package homework1.task2_braces;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BracesCheckerTest {

    @Test
    public void isBalanced() {
        assertThat(BracesChecker.isBalanced("Hello(world)[panda{sadness}extinction]"), is(true));
        assertThat(BracesChecker.isBalanced("Hel((lo(world)))[panda{sadness}extinction]"), is(true));
        assertThat(BracesChecker.isBalanced("(({}[])())"), is(true));
        assertThat(BracesChecker.isBalanced(""), is(true));
        assertThat(BracesChecker.isBalanced("((("), is(false));
        assertThat(BracesChecker.isBalanced("{]([}"), is(false));
    }
}