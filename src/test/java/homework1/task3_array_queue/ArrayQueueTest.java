package homework1.task3_array_queue;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ArrayQueueTest {

    @Test
    public void thatWeCanAddElementsToQueueTest() {
        ArrayQueue<String> queue = new ArrayQueue<>();
        queue.add("hello");
        queue.add("panda");
        queue.add("evil panda");
        assertThat(queue.size(), is(3));
    }

    @Test
    public void thatWeCanRemoveElementsFromQueueTest() {
        ArrayQueue<String> queue = new ArrayQueue<>();
        queue.add("hello");
        queue.add("panda");
        queue.add("evil panda");
        assertThat(queue.remove(), is("hello"));
        assertThat(queue.remove(), is("panda"));
        assertThat(queue.remove(), is("evil panda"));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void thatWeCantDeleteItemsFromEmptyQueueTest() {
        ArrayQueue<String> queue = new ArrayQueue<>();
        queue.remove();
    }

    @Test
    public void peekMethodTest() {
        ArrayQueue<String> queue = new ArrayQueue<>();
        queue.add("hello");
        assertThat(queue.peek(), is("hello"));
        assertThat(queue.size(), is(1));
    }
}