package homework1.task0_linked_list;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class LinkedListTest {

    @Test(expected = NullPointerException.class)
    public void insert() {
        LinkedList<String> list = new LinkedList<>();
        boolean insert = list.insert("Hello");

        assertThat(insert, is(true));
        list.insert(null);
    }

    @Test
    public void size() {
        LinkedList<String> list = new LinkedList<>();
        list.insert("Hello");
        list.insert("sad");
        list.insert("panda");

        assertThat(list.size(), is(3));
    }
}